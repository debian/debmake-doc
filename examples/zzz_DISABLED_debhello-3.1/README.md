# debhello in Python3 (GUI)

Copyright (C) 2021 Osamu Aoki <osamu@debian.org>

More modern PEP517 compliant style package.

We can still rely on old distutils like build system (actually setuptools)
since this comes with minimal setup.py.

 python3 setup.py bdist_wheel

