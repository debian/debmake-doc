# directory structure (abs path)
#
# @ ${EXAMPLES_PATH}
# + - @ ${BUILD_PATH}
#     +- @ ${DSC_PATH}
#        +- @ ${SOURCE_PATH}

echo " \$ wget http://www.example.org/download/${PROJECT}.tar.gz"
echo " ..."
# Reality behind scene
cp -a ${EXAMPLES_PATH}/${PROJECT}.${TGZ} ${DSC_PATH}
CD ${DSC_PATH} >/dev/null
cd ${DSC_PATH}
L "tar -xzmf ${PROJECT}.tar.gz"
L "tree"
