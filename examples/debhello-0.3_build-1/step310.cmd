# @ ${SOURCE_PATH}
# Let's pretend to edit Makefile in source interactively
echo " \$ vim Makefile"
echo " ... hack, hack, hack, ..."
# Let's edit Makefile non-interactively
sed -i -e 's/\/usr\/local/\$(DESTDIR)\/usr/' Makefile
L "cat Makefile"
