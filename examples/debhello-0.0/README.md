# C + Makefile

This is a build example source for C program with `Makefile`.

- Standard Makefile with `prefix`

There are 4 Debianization approaches:

- build-1: use `dh_auto_install -- prefix=/usr` in `debian/rules`
- build-2: use of `debian/patches/000-prefix-usr.patc`  using just `diff -u ...`
- build-3: use of `debian/patches/000-prefix-usr.patc` using `dquilt ...`
- build-4: use of `debian/patches/000-prefix-usr.patc` using `dpkg-source --commit . ...`
