# Fake download session
echo " \$ wget http://www.example.org/download/${PROJECT}.tar.gz"
echo " ..."
# Reality behind scene
cp -a ${EXAMPLES_PATH}/${PROJECT}.${TGZ} ${BUILD_PATH}
CD ${BUILD_PATH} >/dev/null
cd ${BUILD_PATH}
L "tar -xzmf ${PROJECT}.tar.gz"
L "tree"
