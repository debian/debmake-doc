# @ ${SOURCE_PATH}
# Let's pretend to add debian/source/local-options in source interactively
echo " \$ mv debian/source/local-options.ex debian/source/local-options"
echo " \$ vim debian/source/local-options"
echo " ... hack, hack, hack, ..."
# Let's set debian/source/options non-interactively
rm -f debian/source/local-options.ex
cp -f ${EXAMPLES_PATH}/${PROJECT}/debian/source/local-options debian/source/local-options
L "cat debian/source/local-options"
