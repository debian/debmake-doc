# @ ${DSC_PATH}

FILE=$(ls -1 *.debian.tar.?z)
for f in $FILE ; do
EXT=${f##*.}
if [ "$EXT" = "gz" ]; then
L "tar -xzvf $f"
elif [ "$EXT" = "xz" ]; then
L "tar --xz -xvf $f"
else
:
fi
done
