#!/bin/sh -e
# https://docs.asciidoctor.org/asciidoctor/latest/migrate/asciidoc-py/
# ``XXXXX'' "`XXXXX'" double quote
sed -i -e "s/\`\`\([^\`]*\)''/\"\`\1\`\"/g"  "$@"
# 'XXXXX' __XXXXX__ double quote --> used vim since some false match
#sed -i -e "s/'\([a-zA-Z0-9][^']*\)'/__\1__/g"  "$@"
