// vim:set filetype=asciidoc:
[[basics]]
== Basics for packaging

Here, a broad overview is presented without using VCS operations for the basic rules of Debian packaging focusing on the non-native Debian package in the "`**3.0 (quilt)**`" format.

NOTE: Some details are intentionally skipped for clarity.  Please read the manpages of the *dpkg-source*(1), *dpkg-buildpackage*(1), *dpkg*(1), *dpkg-deb*(1), *deb*(5), etc.

The Debian source package is a set of input files used to build the Debian binary package and is not a single file.

The Debian binary package is a special archive file which holds a set of installable binary data with its associated information.

A single Debian source package may generate multiple Debian binary packages defined in the *debian/control* file.

The non-native Debian package in the Debian source format "`**3.0 (quilt)**`" is the most normal Debian source package format.

NOTE: There are many wrapper scripts.  Use them to streamline your workflow but make sure to understand the basics of their internals.

[[workflow]]
=== Packaging workflow

The Debian packaging workflow to create a Debian binary package involves generating several specifically named files (see "`<<name-version>>`") as defined in the "`Debian Policy Manual`".  This workflow can be summarized in 10 steps with some over simplification as follows.

1. The upstream tarball is downloaded as the __package-version__**.tar.gz** file.
2. The upstream tarball is untarred to create many files under the __package-version__**/** directory.
3. The upstream tarball is copied (or symlinked) to the particular filename __packagename_version__**.orig.tar.gz**.
* the character separating __package__ and __version__ is changed from *-* (hyphen) to *_* (underscore)
* *.orig* is added in the file extension.
4. The Debian package specification files are added to the upstream source under the __package-version__**/debian/** directory.
* Required specification files under the *debian/* directory:
*debian/rules*::
The executable script for building the Debian package (see "`<<rules>>`")
*debian/control*::
The package configuration file containing the source package name, the source build dependencies, the binary package name, the binary dependencies, etc. (see "`<<control>>`")
*debian/changelog*::
The Debian package history file defining the upstream package version and the Debian revision in its first line (see "`<<changelog>>`")
*debian/copyright*::
The copyright and license summary (see "`<<copyright>>`")
* Optional specification files under the *debian/** (see "`<<debianconf>>`"):
* The *debmake* command invoked in the __package-version__**/** directory may be used to provide the initial template of these configuration files.
** Required specification files are generated even with the *-x0* option.
** The *debmake* command does not overwrite any existing configuration files.
* These files must be manually edited to their perfection according to the "`https://www.debian.org/doc/debian-policy/[Debian Policy Manual]`" and "`https://www.debian.org/doc/manuals/developers-reference/[Debian Developer's Reference]`".
5. The *dpkg-buildpackage* command (usually from its wrapper *debuild* or *sbuild*) is invoked in the __package-version__**/** directory to make the Debian source and binary packages by invoking the *debian/rules* script.
* The current directory is set as: "`**CURDIR=**__/path/to/package-version/__`"
* Create the Debian source package in the Debian source format "`**3.0 (quilt)**`" using *dpkg-source*(1)
** __package_version__**.orig.tar.gz** (copy or symlink of __package-version__**.tar.gz**)
** __package_version-revision__**.debian.tar.xz** (tarball of **debian/** found in __package-version__**/**)
** __package_version-revision__**.dsc**
* Build the source using "`*debian/rules build*`" into *$(DESTDIR)*
** "`**DESTDIR=debian/**__binarypackage__**/**`" for single binary package footnote:[This is the default up to *debhelper* v13.  At *debhelper* v14, it warns the default change. After *debhelper* v15, it will change the default to **DESTDIR=debian/tmp/** .]
** "`**DESTDIR=debian/tmp/**`" for multi binary package
* Create the Debian binary package using *dpkg-deb*(1), *dpkg-genbuildinfo*(1), and *dpkg-genchanges*(1).
** __binarypackage_version-revision_arch__.*deb*
** ... (There may be multiple Debian binary package files.)
** __package_version-revision_arch__.*changes*
** __package_version-revision_arch__.*buildinfo*
6. Check the quality of the Debian package with the *lintian* command. (recommended)
* Follow the rejection guidelines from https://ftp-master.debian.org/[ftp-master].
** "`https://ftp-master.debian.org/REJECT-FAQ.html[REJECT-FAQ]`"
** "`https://ftp-master.debian.org/NEW-checklist.html[NEW checklist]`"
** "`https://ftp-master.debian.org/#lintianrejects[Lintian Autorejects]`" ("`https://ftp-master.debian.org/static/lintian.tags[lintian tag list]`")
7. Test the goodness of the generated Debian binary package manually by installing it and running its programs.
8. After confirming the goodness, prepare files for the normal source-only upload to the Debian archive.
9. Sign the Debian package file with the *debsign* command using your private GPG key.
* Use "`**debsign** __package_version-revision__**_source.changes**`" (normal source-only upload situation)
* Use "`**debsign** __package_version-revision_arch__**.changes**`" (exceptional binary upload situation such as NEW uploads, and security uploads)
files for the binary Debian package upload.
10. Upload the set of the Debian package files with the *dput* command to the Debian archive.
* Use "`**dput** __package_version-revision__**_source.changes**`" (source-only upload)
* Use "`**dput** __package_version-revision_arch__**.changes**`" (binary upload)

Test building and confirming of the binary package goodness as above is the moral obligation as a diligent Debian developer but there is no physical barrier for people to skip such operations at this moment for the source-only upload.

Here, please replace each part of the filename as:

* the __package__ part with the Debian source package name
* the __binarypackage__ part with the Debian binary package name
* the __version__ part with the upstream version
* the __revision__ part with the Debian revision
* the __arch__ part with the package architecture (e.g., *amd64*)

See also "`https://wiki.debian.org/SourceOnlyUpload[Source-only uploads]`".

TIP: Many patch management and VCS usage strategies for the Debian packaging are practiced.  You don't need to use all of them.

TIP: There is very extensive documentation in "`https://www.debian.org/doc/manuals/developers-reference/best-pkging-practices.html[Chapter 6. Best Packaging Practices]`" in the "`Debian Developer's Reference`".  Please read it.

[[debhelper]]
=== *debhelper* package

Although a Debian package can be made by writing a *debian/rules* script without using the *debhelper* package, it is impractical to do so.  There are too many modern "`https://www.debian.org/doc/debian-policy/[Debian Policy]`" required features to be addressed, such as application of the proper file permissions, use of the proper architecture dependent library installation path, insertion of the installation hook scripts, generation of the debug symbol package, generation of package dependency information, generation of the package information files, application of the proper timestamp for reproducible build, etc.

*Debhelper* package provides a set of useful scripts in order to simplify Debian's packaging workflow and reduce the burden of package maintainers. When properly used, they will help packagers handle and implement "`https://www.debian.org/doc/debian-policy/[Debian Policy]`" required features automatically.

The modern Debian packaging workflow can be organized into a simple modular workflow by:

* using the *dh* command to invoke many utility scripts automatically from the *debhelper* package, and
* configuring their behavior with declarative configuration files in the *debian/* directory.

You should almost always use *debhelper* as your package's build dependency. This document also assumes that you are using a fairly contemporary version of *debhelper* to handle packaging works in the following contents.

NOTE: For *debhelper* "`compat >= 9`", the *dh* command exports compiler flags (*CFLAGS*, *CXXFLAGS*, *FFLAGS*, *CPPFLAGS* and *LDFLAGS*) with values as returned by *dpkg-buildflags* if they are not set previously.  (The *dh* command calls *set_buildflags* defined in the *Debian::Debhelper::Dh_Lib* module.)

NOTE: **debhelper**(1) changes its behavior with time.  Please make sure to read **debhelper-compat-upgrade-checklist**(7) to understand the situation.
