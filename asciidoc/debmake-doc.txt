// vim:set filetype=asciidoc:
= Guide for Debian Maintainers
:Author:    Osamu Aoki
:Email:     osamu@debian.org
:Date:      @@@debdate@@@
:Revision:  @@@debversion@@@
:encoding: UTF-8
// The above date and revision are only used by testing HTML test page within
// this directory.
//
// head.txt generated from 00-terms.txt[] replaces corresponding part.  Thus
// date comes from there.
//####include::00-terms.txt[]

// asciidoc output docbook 4.5 entity but the first conversion creates
// the real UTF-8 output in the XML base file.
//
// ASCIIDOC SOURCE STYLE GUIDE
// '             : leave it to asciidoc to convert to ’ U+2019
//     use for for let's, doesn't don't can't *'s *s' o'clock we'll
//                 U+2019 RIGHT SINGLE QUOTATION MARK (NYT,arstechnica.com, PDF of US.GOV) **
//                 U+0027 APOSTROPHE                  (Wikipedia.org)
//                 U+2032 PRIME
// "quoted text" : avoid using this except in code section
//
// "`quoted text`" : leave it to asciidoc to convert to “quoted text”
//                 U+201C LEFT DOUBLE QUOTATION MARK
//                 U+201D RIGHT DOUBLE QUOTATION MARK
// `quoted text' : leave it to asciidoc to convert to ‘quoted text’
//                 U+2018 LEFT SINGLE QUOTATION MARK
//                 U+2019 RIGHT SINGLE QUOTATION MARK
//
// Problem with LEFT and RIGHT SINGLE QUOTATION MARKs
//   * xetex treat it as full width CJK char.
//   * All Japanese character treats these as double width chars.

[[preface]]
== Preface

include::01-preface.txt[]

[[overview]]
== Overview

include::02-overview.txt[]

// [[start]]
// == Prerequisites

include::10-prereq.txt[]

// [[setup]]
// == Tool Setups

include::12-setups.txt[]

// [[simple]]
// == Simple Example

include::14-simpledeb.txt[]

// [[basics]]
// == Basics

include::20-workflow.txt[]

include::21-version.txt[]

include::22-rules.txt[]

include::23-control.txt[]

include::25-changelog.txt[]

include::26-copyright.txt[]

include::27-patches.txt[]

include::28-keyconf.txt[]

include::29-otherconf.txt[]

include::30-quality.txt[]

include::31-sanitize.txt[]

// [[more]]
// == More on packaging

include::32-more.txt[]

include::34-advanced.txt[]

include::38-git.txt[]

// [[tips]]
// == Tips

include::40-tips.txt[]


// [[tools]]
// == Tools

include::60-tools.txt[]

// [[more]]
// == More Examples

include::70-example.txt[]

include::71-emptypkg.txt[]

include::72-nomake.txt[]

include::73-makesh.txt[]

include::74-pyproject.txt[]

include::75-makesh-gui.txt[]

include::76-pyproject-gui.txt[]

include::77-make.txt[]

include::78-configure.txt[]

include::79-autotools.txt[]

include::80-cmake.txt[]

include::81-libautotools.txt[]

include::82-libcmake.txt[]

include::83-i18n.txt[]

include::84-details.txt[]

[[manpage]]
include::90-manpage.txt[]

// [[options]]
// == debmake options

include::95-options.txt[]
