# TODO

## SITUATION

`...` is converted into `…​` (U+8230 U+200B) Ellipsis + Zerowidth space

Translator are usually expected to put Zerowidth space.

ENGLISH
"<emphasis role="strong">:wq</emphasis>"

CORRECT
<quote><emphasis role="strong">:wq</emphasis></quote>


## VERIFY

HTML and PDF results for <quote>...</quote>

DE
»<emphasis role="strong">dh $@</emphasis>«
ZH
“<emphasis role="strong">make dist</emphasis>”

## CONVERT

????
