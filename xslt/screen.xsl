<?xml version="1.0" encoding="utf-8" ?>
<!-- vim: set sts=2 ai expandtab: -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" indent="yes" encoding="UTF-8"/>

<!-- don't translate screen -->
<xsl:template match="screen">
  <xsl:copy>
    <xsl:text>en</xsl:text>
  </xsl:copy>
</xsl:template>

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>


</xsl:stylesheet>

