# Font for CJK

## Asian font basics (Latex)

Use xeCJK only for CJK http://bugs.debian.org/666569

`latex-cjk-all` contains xeCJK.

If texlive tool chain in Debian/unstable fails to build PDF for CJK, activate NOPDF in debian/rules to work around.

## Note on TTF

In bookworm and later, `fc-list` doesn't pick up some fonts.  Here are lists of actively locatable CJK fonts.

Some fonts has been moved to non-free (No more IPA non-P fonts)

ゴシック Gothic == Sans  == 黒体　黑體
明朝     Mincho == Serif == 宋体

## Accessible TTF fonts

### Japanese: fonts-vlgothic
```
/usr/share/fonts/truetype/vlgothic/VL-PGothic-Regular.ttf: VL PGothic,VL Pゴシック:style=regular
/usr/share/fonts/truetype/vlgothic/VL-Gothic-Regular.ttf: VL Gothic,VL ゴシック:style=regular
```

### Japanese: fonts-ipafont
```
/usr/share/fonts/opentype/ipafont-gothic/ipagp.ttf: IPAPGothic,IPA Pゴシック:style=Regular
/usr/share/fonts/opentype/ipafont-mincho/ipamp.ttf: IPAPMincho,IPA P明朝:style=Regular
```

### Chinese: fonts-wqy-zenhei
```
/usr/share/fonts/truetype/wqy/wqy-zenhei.ttc: WenQuanYi Zen Hei,文泉驛正黑,文泉驿正黑:style=Regular
/usr/share/fonts/truetype/wqy/wqy-zenhei.ttc: WenQuanYi Zen Hei Sharp,文泉驛點陣正黑,文泉驿点阵正黑:style=Regular
/usr/share/fonts/truetype/wqy/wqy-zenhei.ttc: WenQuanYi Zen Hei Mono,文泉驛等寬正黑,文泉驿等宽正黑:style=Regular
```

### Chinese: fonts-arphic-uming
```
/usr/share/fonts/truetype/arphic/uming.ttc: AR PL UMing TW MBE:style=Light
/usr/share/fonts/truetype/arphic/uming.ttc: AR PL UMing TW:style=Light
/usr/share/fonts/truetype/arphic/uming.ttc: AR PL UMing CN:style=Light
/usr/share/fonts/truetype/arphic/uming.ttc: AR PL UMing HK:style=Light
```

### Korean: fonts-baekmuk
```
/usr/share/fonts/truetype/baekmuk/batang.ttf: Baekmuk Batang,백묵 바탕:style=Regular
/usr/share/fonts/truetype/baekmuk/hline.ttf: Baekmuk Headline,백묵 헤드라인:style=Regular
/usr/share/fonts/truetype/baekmuk/dotum.ttf: Baekmuk Dotum,백묵 돋움:style=Regular
/usr/share/fonts/truetype/baekmuk/gulim.ttf: Baekmuk Gulim,백묵 굴림:style=Regular
```

* batang.ttf: serif
* dotum.ttf: sans-serif
* gulim.ttf: sans-serif (rounded)
* hline.ttf: headline

### Language non specific Noto font

fonts-noto-cjk (TTC packs JP, KR, SC, TC variants, fc only picks JP as below)

```
/usr/share/fonts/opentype/noto/NotoSansCJK-Bold.ttc: Noto Sans CJK JP:style=Bold
/usr/share/fonts/opentype/noto/NotoSansCJK-Bold.ttc: Noto Sans Mono CJK JP:style=Bold
/usr/share/fonts/opentype/noto/NotoSansCJK-Regular.ttc: Noto Sans CJK JP:style=Regular
/usr/share/fonts/opentype/noto/NotoSansCJK-Regular.ttc: Noto Sans Mono CJK JP:style=Regular
/usr/share/fonts/opentype/noto/NotoSerifCJK-Bold.ttc: Noto Serif CJK JP:style=Bold
/usr/share/fonts/opentype/noto/NotoSerifCJK-Regular.ttc: Noto Serif CJK JP:style=Regular
```

Package description (fontforge can read this)

  *  Noto Sans CJK JP
  *  Noto Sans CJK KR
  *  Noto Sans CJK SC
  *  Noto Sans CJK TC
  *  Noto Sans Mono CJK JP
  *  Noto Sans Mono CJK KR
  *  Noto Sans Mono CJK SC
  *  Noto Sans Mono CJK TC
  *  Noto Serif CJK JP
  *  Noto Serif CJK KR
  *  Noto Serif CJK SC
  *  Noto Serif CJK TC

Sans:思源黑体, Sans Mono:思源等宽黑体, Serif:思源宋体


## Note

### language config

All existing fonts are "12" means "Debian GNU/Linux 12 (bookworm) compatible" to build PDF.

* `xetex_param.xsl` -- active configuration all free sans


## Font info (old)

### Sanserif (OLD list)

* simplified Chinese: 黑体 hēi tǐ
  * fonts-wqy-zenhei (zh_CN centric)
    * WenQuanYi Zen Hei,文泉驛正黑,文泉驿正黑
    * WenQuanYi Zen Hei Sharp,文泉驛點陣正黑,文泉驿点阵正黑
    * WenQuanYi Zen Hei Mono,文泉驛等寬正黑,文泉驿等宽正黑
  * traditional Chinese: 黑體 hēi tǐ
  * Japanese: ゴシック kaku goshikku, gothic
    * fonts-ipafont-gothic (ja centric)
      * IPAPGothic,IPA Pゴシック
        * No more *** IPAGothic,IPAゴシック non-free ???
    * fonts-vlgothic (ja centric)
      * VL PGothic,VL Pゴシック:style=regular
      * VL Gothic,VL ゴシック:style=regular
  * Korean:
    * Baekmuk Dotum,백묵 돋움:style=Regular        ... san-serif

### Serif (Old list)

* 中国大陆一般称：宋体；台湾香港一般称：明體
  * simplified Chinese: 宋体/(明体), Sòngtǐ
    * fonts-arphic-gbsn00lp (zh_CN centric)
      * AR PL SungtiL GB
  * traditional Chinese: (宋體)/明體, Sòngtǐ
    * fonts-arphic-bsmi00lp (zh_TW centric)
      * AR PL Mingti2L Big5
        * (Missing some character in output...)
    * fonts-arphic-uming    (zh_TW centric)
      * AR PL UMing
        * = "AR PL Mingti2L Big5" and "AR PL SungtiL GB" + extra in Taiwan-style
        * (BUild failure happened thus not used)
  * Japanese: 明朝体, Minchōtai
    * fonts-ipafont-mincho (ja centric)
      * IPAPMincho,IPA P明朝:style=Regular
        * No more *** IPAMincho,IPA明朝: - non-free ???
  * Korean: Hangul: 명조체; Hanja: 明朝體; Revised Romanization: Myeongjoche
    * Baekmuk Batang,백묵 바탕:style=Regular       ... serif

### Script etc.

* simplified Chinese: 楷书; kǎishū
  * fonts-arphic-gkai00mp (zh_CN centric)
    * AR PL KaitiM GB
* traditional Chinese: 楷書; kǎishū
  * fonts-arphic-bkai00mp (zh_TW centric)
    * AR PL KaitiM Big5
  * fonts-arphic-ukai     (zh generic)
    * AR PL UKai
    * = AR PL KaitiM Big5" + "AR PL KaitiM GB" + extra in Taiwan-style
* Japanese: 楷書, kaisho (教科書体)
* Korean:
  * fonts-baekmuk 1st most popular    which contains 4 font families.
    * Baekmuk Batang,백묵 바탕:style=Regular       ... serif
    * Baekmuk Dotum,백묵 돋움:style=Regular        ... san-serif
    * Baekmuk Gulim,백묵 굴림:style=Regular        ... san-serif  (round)
    * Baekmuk Headline,백묵 헤드라인:style=Regular ... bold san-serif    (best screen small)
  * fonts-unfonts-core    2nd popular and increasing
                    made from the HLaTeX's PostScript fonts
                    UnBatang, UnDotum, Ungraphic, Unpilgi, and UnGungseo
    * UnBatang  ... serif
    * UnDotum   ... san-serif
    * UnGungseo ... Kai/kaisho
                    latex-cjk-xcjk uses UnBatang as example

  * fonts-alee    3rd most popular    many fonts by alee
  * fonts-nanum                       Myeongjo and Gothic Korean font families/screen font
  * fonts-nanum-coding                Korean fixed width font family

### encoding names

This is largely obsoleted and replaced by UTF-8 on modern Linux systems.

  * GB2312, GBK or GB18030: Simplified Chinese
  * Big5:                   Traditional Chinese

